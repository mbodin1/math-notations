Les références entre parenthèses sont les références de l’ISO 80 000-2.

* Quantificateurs

- [ ] ∀ x, … (2-4.6)
- [ ] ∀ x ∈ E, … (2-4.6)
- [ ] ∀ x ∉ E, …
- [ ] ∀ x > 0, …
- [ ] ∀ 0 < x < 1, …
- [ ] ∀ E ⊂ F, … (F libre)

- [ ] ∀ (x, y) ∈ E², …
- [ ] ∀ x, y, …
- [ ] ∀ x, y ∈ E, …
- [ ] ∀ x, y > 0, …
- [ ] ∀ 0 < x < y < 1, …
- [ ] ∀ x ≠ y, …
- [ ] ∀ E ⊂ F, … (F lié) Est-ce qu’on écrirait pas plutôt ∀ E ⊂ F ⊂ K, … ?

- [ ] ∀ f : A → B, …
- [ ] ∀ f, g : A → B, …

- [ ] Variantes avec ∃ (2-4.7)
```coq
Syntax exists x t := "'exists' x, t" (at level 200, x iterable binder, right associativity).
```
- [ ] Variantes avec ∃! (variante : ∃¹) (2-4.7)

- [ ] « Pour tout x₁, …, x\_n distinct deux à deux »

* Ensembles

- [ ] \{ x + y | x ∈ E, y ∈ F \}
- [ ] \{ (x, y) | x ≥ y \}
- [ ] \{ x ∈ E | P(x) \} (2-5.4)
- [ ] \{ x | P(x) \} (2-5.4)
- [ ] \{ f(x) | x ∈ E\} (2-5.3 ?)
- [ ] \{ x : T | P (x) \}
- [ ] Variantes avec des virgules, des points-virgule, ou des double points : \{ f(x), x ∈ E\}, \{ f(x); x ∈ E\}, \{ f(x) : x ∈ E\}

- [ ] A × B (2-5.16)
- [ ] A - B, A ∖ B (2-5.13)
- [ ] A + B
- [ ] A^B, ℱ (E, F)
- [ ] A^n (2-5.17)
- [X] A ∪ B (2-5.9)
```coq
Syntax union1 A B := "A \union B" (at level 49, right associativity).
Syntax union2 A B := "A ∪ B" (at level 49, right associativity).
```
- [ ] A ∩ B (2-5.10)
- [ ] A ⊎ B (union disjointe, mais aussi union de multi-ensembles), x ⊔ y
- [ ] A # B
- [ ] A / B (ensembles quotients)

- [ ] A ⊆ B, B ⊇ A (2-5.7)
- [ ] A ⊊ B, B ⊋ A (2-5.8)
- [ ] A ⊂ B, B ⊃ A, avec un sens ambigüe qui peut être soit ⊆, soit ⊊. (2-5.8)
- [ ] A ⊈ B, B ⊉ A, A ⊄ B, B ⊅ A

- [ ] \(a, b\), \(a; b\), \(a | b\) (2-5.14)
- [ ] \(v₁, …, v\_n\), \(v₁; …; v\_n\), \(v₁ | … | v\_n\) (2-5.15)

- [ ] id, id\_A = \{ (x, x) | x ∈ A \} (2-5.18)

- [X] x ∈ E, E ∋ x (2-5.1)
```coq
Syntax is_in1 x E := "x ∈ E" (at level 39).
Syntax is_in2 x E := "x \in E" (at level 39).
```
- [ ] x ∉ E, E ∌ x (2-5.2)
- [X] Ø, ∅, \{\} (2-5.6), \{x\}
```coq
Syntax empty1 := "\emptyset".
Syntax empty2 := "∅".
Syntax empty3 := "\{}".
Syntax empty4 := "\{ }".
Syntax single x := "\{ x }".
```
- [ ] \{ x₁, …, x\_n \} (2-5.3)
- [ ] P(A) (parfois notée ℙ(A))
- [ ] Complémentaire : Ā, ∁A, ^C A, A^C
- [ ] Variante avec l’ensemble : ∁\_E A (= E - A).

- [ ] ∩\_{i = 1\}^n … (2-5.12)
- [ ] ∩\_{i = 0\}^\{+∞\} …
- [ ] ∩\_{i ≥ 0\} …
- [ ] ∩\_{i ∈ E\} … (2-5.12)
- [ ] ∩\_i …

- [ ] Variantes avec ∪ (2-5.11)
- [ ] Variantes avec ⊎, ⊔
- [ ] Variantes avec le produit cartésien Π (2-5.17)

- [ ] Cardinal : Card (F), |F| (2-5.5)

- [ ] Fonction caractérisque d’un ensemble : δ\_E, 𝟙\_E

- [ ] ℝ (2-6.4), ℝ\_+, ℝ\_-, ℝ\* (2-6.4), ℝ◌̄
- [ ] ℕ (2-6.1), ℕ\_+, ℕ\_-, ℕ\* (2-6.1), ℕ◌̄
- [ ] ℤ (2-6.2), ℤ\_+, ℤ\_-, ℤ\* (2-6.2), ℤ◌̄
- [ ] ℚ (2-6.3), ℚ\_+, ℚ\_-, ℚ\* (2-6.3), ℚ◌̄
- [ ] ℕ\_\{> 5\} (2-6.1), ℤ\_\{≥ -3\} (2-6.2), ℚ\_\{< 0\} (2-6.3), ℝ\_\{≥ 0\} (2-6.4)
- [ ] ℂ, ℂ\* (2-6.5)
- [ ] Ensemble des nombres premiers : ℙ (2-6.6)
- [ ] Ensemble des nombres complexes de module 1 : 𝕌
- [ ] Racines n^ièmes de l’unité : 𝕌\_n

* Minimums et maximums

- [ ] min\_\{i = 0\}^1 …
- [ ] min\_\{i = 0\}^\{+∞\} …
- [ ] min\_\{i ≥ 0\} …
- [ ] min\_\{i ∈ E\} …
- [ ] min\_\{i\} …
- [ ] min\_\{P(i)\} …

- [ ] min\_\{ \[ a, b \] \} f

- [ ] min E (2-9.21)

- [ ] min (a, …, b) (2-9.21)

- [ ] Variantes avec max (2-9.22)
- [ ] Variantes avec inf (2-9.14), sup (2-9.15)
- [ ] Variantes avec argmax, argmin
- [ ] Variantes avec plus d’un indice

* Arithmétique

- [ ] Σ\_\{n = 0\}^n … (2-9.7)
- [ ] Σ\_\{n = 0\}^\{+∞\} …
- [ ] Σ\_\{n ≥ 0\} …
- [ ] Σ\_n a\_n (2-9.7)
- [ ] Σ a\_n (somme sur une suite avec indice implicite) (2-9.7)
- [ ] Σ a\_n (série)

- [ ] Variantes avec le produit Π (2-9.8)

- [ ] x + y (2-9.1)
- [ ] x - y (2-9.2)
- [ ] -x

- [ ] x × y (2-9.5)
- [ ] x·y (2-9.5)
- [ ] x y (2-9.5)
- [ ] xy (2-9.5)

- [ ] x / y (2-9.6)
- [ ] x : y (2-9.6)
- [ ] x ÷ y (déconseillé par 2-9.6)

- [ ] x ± y (2-9.3)
- [ ] x ∓ y (2-9.4)

- [ ] Disivibilité : a | b, a ∣ b (2-7.17)

- Valeur absolue |a| (2-9.14)

- [ ] Constantes (0, 1, 1.5, 1., π (2-13.1), etc.)

- [ ] n! (2-10.1)
- [ ] a^\{\\underbar{k}\}, [a]\_k (2-10.2)
- [ ] a^\{\\overbar{k}\}, (a)\_k (2-10.3)

- [ ] x ≡ y \[k\], x≡ y mod k (2-7.18), x = y \[k\], x = y mod k

- [ ] Coefficients binomiaux (en conflit avec une des notations vecteur, 2-10.4)
- [ ] C^k\_n (2-10.6), ^RC^k\_n (2-10.7), V^k\_n (2-10.8), ^RV^k\_n (2-10.9)
- [ ] B\_n (2-10.5)

- [ ] Symbole de Kronecker (= 1 si i = j et 0 sinon) : δ\_\{i, j\}
- [ ] Signe : sgn a (2-9.13)

* Listes

- [ ] \(x\_n\)
- [ ] \(x\_n\)\_n
- [ ] \(x\_n\)\_\{n ∈ ℕ\}
- [ ] \(f(n)\)\_\{n ∈ ℕ\}

- [ ] x\_i
- [ ] x\_\{i, j\}
- [ ] x\_\{ij\}

* Intervalles

- [ ] \[x, y\] (2-6.7)
- [ ] \[x; y\] (2-6.7 ?)
- [ ] \]x, y\] (2-6.8), \]x, y\[ (2-6.10), \]x; y\] (2-6.8 ?), \]x; y\[ (2-6.10 ?)
- [ ] \(x, y\] (2-6.8), \(x, y\) (2-6.10), \(x; y\] (2-6.8 ?), \(x; y\) (2-6.10 ?)
- [ ] \]-∞, x\] (2-6.11), \[x, +∞\[ (2-6.13), \[x, +∞\) (2-6.13), \]-∞, +∞\[, etc. (2-6.9, 2-6.12, 2-6.14)
- [ ] \[x, ∞\[ (2-6.13), \[x, ∞\) (2-6.13)
- [ ] Variantes dans ℝ◌̄ : \[-∞, +∞\]

- [ ] \[\[p, n\]\] (intervalle d’entiers)

* Fonctions

- [ ] f(x) (2-11.2)
- [ ] x \\to^f y (2-11.5)

- [ ] dom f, D(f)
- [ ] ran f, R(f), Im f, ℑ f. Note : ran (range) peut aussi signifier le codomaine de f et non son image.

- [ ] \(x, y\) ∈ f (coercion vers un ensemble en prenant le graphe)
- [ ] f(E) (coercion vers la fonction sur les parties)
- [ ] Identification de f avec f(x)

- [ ] g ∘ f (2-11.7)
- [ ] f\_\{|E\}
- [ ] f^\{-1\}
- [ ] id, id\_A

- [ ] f : E → F (2-11.3)
- [ ] f : x ⟼ f(x)
- [ ] f : x ⟼ f(x), x ∈ E (2-11.4)
- [ ] f : E → F, x ⟼ f(x)

- [ ] Surjections f : A ->> B
- [ ] Injections f : A >-> B
- [ ] Bijections f : A >->> B

- [ ] C(E, F), 𝒞 (E, F), 𝒞^k (E, F)
- [ ] CM(E, F), 𝒞ℳ (E, F) (fonctions continues par morceaux)
- [ ] 𝒞\_p : fonctions à valeurs complexes continues sur ℝ et p-périodiques.
- [ ] 𝒞^ℳ \_p : fonctions à valeurs complexes continues par morceaux sur ℝ et p-périodiques.
- [ ] B(E, F), ℬ(E, F) (fonctions bornées de E dans F)

* Analyse

- [ ] Racine carré : √x (2-9.10), sqrt(x), racine k^ième (2-9.11)
- [ ] sin x (2-13.2), cos x (2-13.3), tan x (2-13.4), cot x (2-13.5), sec x (2-13.6), csc x, cosec (2-13.7)
- [ ] sinh x (2-13.14), cosh x (2-13.15), tanh x (2-13.16) (et les variantes sh, ch, th), etc. (2-13.17 jusqu’à 2-13.25)
- [ ] arcsin (2-13.8), asin, asn (et les variantes pour cos (2-13.9), tan (2-13.10), etc. (2-13.11, 2-13.12, 2-13.13))

- [ ] a^b (2-9.9 (pour b entier), 2-12.2 (pour b réel))

- [ ] exp (2-12.3), e (2-12.1)

- [ ] ln (2-12.5), log (2-12.4), ln\_e, log\_e, log\_2, log\_10 (2-12.4), lg (2-12.6), lb (2-12.7)

- [ ] ∞, +∞, -∞ (2-7.15)
- [ ] ⌊x⌋ (2-9.17), floor x, ⌈x⌉ (2-9.18), ceil x
- [ ] int x (2-9.19), frac x (2-9.20)

- [ ] ∫ f(x) dx (2-11.18)
- [ ] ∫\_0^1 f(x) dx (2-11.19)
- [ ] ∫\_\{-∞\}^\{+∞\} f(x) dx
- [ ] ∫\_\{x = 0\}^1 f(x) dx
- [ ] ∫\_J f(x) dx
- [ ] ∫\_J f

- [ ] Variantes avec ∬, ∭, ∮, ∯, ∰ (2-11.19)
- [ ] Valeur principales de Cauchy (plusieurs notations présentes sur Wikipédia, l’ISO 80 000-2 en référence peu : 2-11.20 et 2-11.21)

- [ ] \[ f(x) \]\_0^1
- [ ] \[ f(x) \]\_\{x = 0\}^1
- [ ] f|^a\_b (2-11.6)
- [ ] f(…,x,…)|^\{x=a\}\_\{x=b\} (2-11.6)

- [ ] f', D f, df/dx, df(x)/dx (2-11.12)
- [ ] \(df/dx\)\_\{x = a\}, f'(a) (2-11.13)
- [ ] f^\{(k)\}, D^k f, d^k f/dx^k (2-11.14)
- [ ] En physique : ḟ pour la dérivée sur le temps (2-11.12)
- [ ] ∂f/∂x, ∂f/∂x\_i, D\_i f, D\_x f(…, x, …), ∂\_x f, (∂f/∂x)\_y, ∂f(…,x,…)/∂x (2-11.15)
- [ ] ∂^k f/∂x\_1^r…∂x\_n^s
- [ ] D\_h f, dérivée selon le vecteur h (f est une fonction à plusieurs variables)
- [ ] d f(a) (différentielle de f en a) (2-11.16)
- [ ] δ f (2-11.17)
- [ ] grad f (a)
- [ ] Δf (2-11.11)
- [ ] ∇²f

- [ ] lim\_\{ x → ∞ \} f(x) (2-11.8)
- [ ] lim\_\{ n → ∞ \} a\_n, lim a\_n, lim\_n a\_n

- [ ] o(a\_n), O(a\_n) pour les suites
- [ ] ~ pour les suites
- [ ] Variantes ~^\{ n → ∞ \}, =^\{ n → ∞ \}, = O\_\{ n → ∞ \}, = o\_\{ n → ∞ \}

- [ ] o(f(x)), O(f(x)) pour les fonctions (2-11.10, 2-11.9)
- [ ] Variante f(x) = o(f(x)) lorsque x → a, f(x) = O(g(x)) lorsque x → a (2-11.10, 2-11.9)
- [ ] Variante f = o(g), f = O(g)
- [ ] ~ pour les fonctions
- [ ] Variantes ~^\{ x → ∞ \}, =^\{ x → ∞ \}, = O\_\{ x → ∞ \}, = o\_\{ x → ∞ \}
- [ ] Variantes ~^\{ x → v \}, =^\{ x → v \}, = O\_\{ x → v \}, = o\_\{ x → v \}
- [ ] Variantes avec x → v+ ou x → v- (2-11.8)
- [ ] Variantes avec Θ, Ω et ω
- [ ] Variantes ≪  (et ≫)
- [ ] Variantes (désuète) ≺ et ≼
- [ ] Variantes f(x) ∈ O(g(x))
- [ ] Abus de notations de type o(g) - o(g) = o(g) (qui ont une interprétation ensembliste cohérente)

- [ ] x → a (2-7.16), f(x) → a lorsque x → b (2-11.8)

- [ ] Probabilité ℙ(P(X)) (parfois simplement notée P, parfois avec des crochets plutôt que des parenthèses)
- [ ] Espérance 𝔼(f(X)) (parfois simplement notée E, parfois avec des crochets)

- [ ] Partie entière E(x).

- [ ] c\_n(f), f^\\^\_n (coefficients de Fourier)
- [ ] H\_n(f) (harmonique de rang n)
- [ ] S\_n(f) (série de Fourier)
- [ ] S\_n(f)(x) (somme partielle des coefficients de Fourier)
- [ ] a\_n(f), b\_n(f) (coefficients de Fourier réels)

* Polynomes

- [ ] K\[X\], 𝕂\[X\]
- [ ] K\_n\[X\], 𝕂\_n\[X\]

* Logique

- [ ] A => B, B <= A, A -> B (2-4.4), A <=> B, A <-> B (2-4.5), A ⊃ B
- [ ] A /\\ B, A ∧ B (2-4.1)
- [ ] A \\/ B, A ∨ V (2-4.2)
- [ ] ~ A, ¬ A (2-4.3)
- [ ] ⊕, ⊻ (xor)

- [ ] x ⋆ y, x * y
- [ ] ★\_\{i = 0\}^n …
- [ ] -\*

- [ ] emp
- [ ] eṁp

* Géométrie

- [ ] Notation vecteur AB^→ (2-8.5)
- [ ] Segments, droites, et demi-droites \[AB\], \(AB\), \[AB\), \]AB\) (À noter que l’on peut aussi nommer une droite (d) ou Δ)
- [ ] Variante de notation : \\overbar{AB} (2-8.4)
- [ ] Triangles (ABC), Quadrilatère (ABCD)
- [ ] Notations angles ^ABC, ∠ ABC ∡ ABC, ∢ ABC (2-8.3)
- [ ] Arcs \\wideparen\{AB\}

- [ ] Distance d(A, B) (2-8.6)
- [ ] Produit scalaire ·, ( . | . ), ou < . | . >
- [ ] tensoriel ⊗
- [ ] Perpendiculaire/orthogonal ⊥ (2-8.2), parallèle ∥ ou // (2-8.1)

- [ ] |v| (norme d’un vecteur, 2-9.16)
- [ ] ‖x‖\_1, ‖x‖
- [ ] N₂, ‖x‖\_2
- [ ] N\_∞, ‖x‖\_∞

- [ ] P ∈ Δ (unification de la droite avec l’ensemble de points correspondant)

- [ ] Complexe : i (2-14.1), j (2-14.1)
- [ ] Quaternions : i, j, k

- [ ] Conjugué \\overbar{z}, z\* (2-14.6)
- [ ] Partie réelle Re z (2-14.2, aussi noté ℜ z), imaginaire Im z (2-14.3, aussi ℑ z)
- [ ] arg z : un argument quelconque d’un nombre complexe z (à formaliser par un ensemble ?)
- [ ] arg z : l’argument normalisé (dans \]-π, π\]) de z (2-14.5)
- [ ] |z| (module d’un complexe, 2-9.14, 2-14.4)
- [ ] sgn z (2-14.7, le complexe unitaire le plus proche de z, ou 0 pour 0)
- [ ] 𝕌 pour l’ensemble des nombres complexes de module 1.
- [ ] 𝕌_k pour les racines k-ième de l’unité.

- [ ] Fonction Γ (généralisation de la factorielle dans le plan complexe)

* Algèbre

- [ ] Matrices, Vecteurs
- [ ] Ensemble des matrices de taille m par n : 𝕄(m, n), ℳ\_\{m, n\}, ℳ\_\{m, n\}(𝕂)
- [ ] Matrices carrées : 𝕄(n), ℳ\_\{n\}, ℳ\_\{n\}(𝕂)
- [ ] dim E
- [ ] k-ième composante d’un vecteur : e^k

- [ ] L₁(I), L₂(I)

- [ ] F^⊥ (orthogonal d’un espace vectoriel)
- [ ] O(E) (groupe orthogonal)
- [ ] O(n) ou O\_n(ℝ) (groupe des matrices orthogonales)

- [ ] Σ, ⊕ (somme et somme directe d’espace vectoriels)
- [ ] ℒ(E, F), L(E, F) : applications linéaires
- [ ] ℒ(E), L(E) : endomorphismes
- [ ] Ker f, Im f : noyau et image
- [ ] Variante de l’image : f(E) où E est le domaine de f.
- [ ] Det A, Det u (déterminant d’une matrice ou d’un endomorphisme)
- [ ] Rg A, Rg u (rang)
- [ ] Tr A, Tr u (trace, parfois noté sans espace ni parenthèses après Tr)
- [ ] Sp(A), Sp(u) (spectre)
- [ ] E\_λ(A), E\_λ(u) (sous-espace propre)
- [ ] m\_λ(A), m\_λ(u) (ordre de multiplicité)
- [ ] χ\_λ(A), χ\_λ(u) (polynome caractéristique)
- [ ] Π\_λ(A), Π\_λ(u) (polynome minimal)

* Programmation

- [ ] begin t end
- [ ] \{ t \}
- [ ] \{ x₁ = v₁; … \}
- [ ] x = 5  (c assignement)
- [ ] x <- 5, x := 5, 5 -> x (variantes d’affectations, la dernière est utilisée en R)
- [ ] x += 5, x -= 5, x \*= 5, x /= 5, x |= 5, x &= 5, x ^= 5, x %= 5, x >>= 5, x <<= 5
- [ ] \{ x₁ : T₁; … \}
- [ ] ;   (point virgule optionnel en fin de ligne comme en JS, ou en fin de bloc comme en OCaml)
- [ ] let x = t₁ in t₂
- [ ] fun x₁…x\_n => t
- [ ] λx.t, λx·t, λxy.t, λx, y.t
- [ ] t₁ t₂
- [ ] t₁ ; t₂
- [ ] if e then t₁ else t₂
- [ ] if e then t
- [ ] \(\)
- [ ] ref, !x

- [ ] E |- t : T
- [ ] |- t : T
- [ ] E,F ; S |- t : T
- [ ] S |- m
- [ ] x::t   (notation x a le type t en isabelle)
- [ ] x : t (notation x a le type t en coq/caml)
- [ ] M :> N  (sous typage)

- [ ] t → t'
- [ ] t/s → t'/s'
- [ ] t/s →^T t'/s' (trace)
- [ ] t/s ==> v/s', t/s ⇓ v/s' (big step)
- [ ] Variantes avec une virgule : t, s → t', s'

- [ ] M.f  (module name prefix)
- [ ] m::f  (module name prefix)
- [ ] t.f  (field access)
- [ ] t-\>f  (notation d'accès en C)

- [ ] \[|x;y;z|\]  (array literal)
- [ ] \[x;y;z\]  (list literal)
- [ ] \(x,y,z\)  (tuple)
- [ ] t[i]  (tableau)
- [ ] t.[i]  (string en caml)
- [ ] t.(i) (tableau en caml)

- [ ] match .. with ...
- [ ] fun x -> t  (fonction caml, alors que => en coq)
- [ ] function($name){body}   fonction anonyme php
- [ ] \[..\](float a, float b) { body }  fonction anonyme C++

- [ ] $x  (variable en bash)
- [ ] ${x} (idem)
- [ ] $(x)  (variable en make)
- [ ] `x`   (escape)

- [ ] "..", """.."""  (string notation, problème des \\)
- [ ] '.'  (char notation)

- [ ] &&, &, and (parfois bits à bits, parfois non)
- [ ] ||, |, or
- [ ] ^, xor
- [ ] ~ (en C : inversion des bits)
- [ ] +, -, \*, /
- [ ] +., -., \*., /. (ocaml float op)
- [ ] mod, %
- [ ] <>  (ocaml neq)
- [ ] !=  (C neq, ocaml ptr neq)
- [ ] ==  (boolean comparison)
- [ ] **   (power, some languages)
- [ ] //   (integer div, some languages)
- [ ] ^  (power or string concat)
- [ ] @  (list concat in ocaml)
- [ ] ++ (list concat in haskell)
- [ ] + (list concat in Python)
- [ ] >>, << (décalages de bits)

- [ ] t\[i := x\]  (map update)
- [ ] \[x -> v\]t  (prefix substitution)
- [ ] t\[v/x\]  (postfix substitution, parfois aussi prefix)
- [ ] Variantes : t\[x/v\], t\[x <- v\]
- [ ] Variantes t\<x := v\>

* Général

- [ ] a = b (2-7.1)
- [ ] a ≡ b (2-7.1)
- [ ] a ≠ b (2-7.2)
- [ ] a ≔ b, a := b, a =\_\{def\} b, a ≝ b (2-7.3), a ≜ b
- [ ] a ≙ b (2-7.4)
- [ ] a ≈ b (2-7.5), a ≃ b
- [ ] a(x) ≃ b(x) lorsque x → v (2-7.6)
- [ ] a ~ b, a ∝ b (2-7.7)
- [ ] M ≅ N (2-7.8)
- [ ] a < b (2-7.9), b > a (2-7.10), a ≤ b (2-7.11), b ≥ a (2-7.12) 
- [ ] a ≪ b (2-7.13), a << b
- [ ] b ≫ a (2-7.14), a >> b
- [ ] x ⋘ y, x <<< y

- [ ] Moyenne arithmétique : \\overbar{x}, \<x\>, \\overbar{x}\_a (2-9.12)

- [ ] Parenthèses : \(e\), \[e\], {e}, \<e\> (2-7.19, peu recommandée)

- [ ] Chaînes d’égalités / inégalité : a = b ≠ 0, a := b ≠ 0, a := b = c, a := b < 0, etc.
- [ ] Chaînes d’équivalences : A <=> B <=> C (qui n’est au passage pas (A <=> B) <=> C malgré le fait que l’opération d’équivalence soit associative).

* Noms

- [ ] \overbar{x} peut être un nom différent de x.
- [ ] x\_i peut être un nom.
- [ ] x^i aussi.
- [ ] Est-ce ab est un nom ou a et b avec un opérateur ?
- [ ] 𝕂 (pour un corps quelconque)
- [ ] 𝔹 (pour une boule)

- [ ] Les mathématiques (et c’est confirmé par l’ISO 80 000-2) est basé sur le type de police de caractères :
			- italique pour les variables et définitions locales,
			- droit pour les fonctions génériques (sin, exp, ln, Γ), et les opérateurs (le d de df/dx par exemple).
- [ ] L’ISO 80 000-2 indique que les opérateurs binaires doivent être précédés et suivis d’espaces. Il n’y a donc pas besoin de désambiguer des suites d’opérateurs.

- [ ] Le shadowing est souvent interdit en math.
      Trois modes dans la déclaration d’une notation :
      - Forcer qu’une variable soit libre.
      - Forcer qu’une variable soit liée.
      - Accepter le shadowing.

