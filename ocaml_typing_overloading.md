
# Four important limitations of OCaml type-checking

## Overloading of record names

Until a few years ago, OCaml disallowed declaring two record types with common field names.
Because prefixing field names to avoid name clashes adds very much noise in the code, the
possibility of overloading field names was introduced. However, the resolution process,
which is guided by types, depends on the internals of the type inference algorithm.
In particular, it can depend on the order in which unifications are performed.

## Overloading of operators

Arithmetic expressions in OCaml involve nonstandard notation, e.g. `a +. b`,
because the type system of the language provides no support for overloading
of, e.g., `a + b`. In practice, this limitation leads to significant noise in
code with formulae mixing integer values, big-int values, double values, vectors
and matrices. Also useful would be the overloading of constants, e.g. `0` could
mean a zero vector or a zero matrix. More generally, we may want to overload
operators, e.g. `t.[i]`, to avoid using different syntax for accessing into a
string, into an array, a list, a map, etc.

## Overloading of function names

Consider a list `xs` which is known from the context to be of type `int list`, and
consider the expression `List.map f xs`. In this expression, the name of the `List`
module is redundant: `map f xs` would unambiguously refer to the only `map` operation
defined on the type `list`. Most programming languages take advantage of this property
to obtain more concise code. In OCaml however, we need a lot of redundant information.

Side comment about syntax: when dropping the module name `List`, it makes sense
to offer the common syntax `xs.map(f)` as an alternative for `map f xs`. This syntax
is nice for chaining operations concisely, e.g., `xs.map(f).map(g)`. It is also nice
for mutable containers, e.g. `q.push(v)`, making it clearer what is modified.

## Error-messages

Cryptic type error messages are a major obstacle to learning OCaml or other ML-based languages.
In many cases, error messages cannot be interpreted without a sufficiently-precise model of
the type inference algorithm. The issues stems, in large part, from the left-to-right bias
associated with unifying arguments or branches one after the other, as opposed to type-checking
the them (almost) independently of each other. The problem and a proposed solution are described
in this paper: http://www.chargueraud.org/research/2015/ocaml_errors/ocaml_errors.pdf



# One solution to overcome all these limitations


## Overview of the solution

We propose to specify a type inference algorithm that is:
- supports resolution of overloaded record fields, and of overloaded
  function names, including arithmetic operations
- is easier to explain and improves error messages.

Nothing comes from free, of course. To work well, our proposed algorithm assumes that,
when typechecking a subexpression, the type of all free variables in known.
To begin with, let us explain how these assumptions can be realized in practice at
a small cost. Then, we'll explain what changes to ML type inference are needed to
achieve our goal.


## Realizing the assumption

The goal is to determine the type of each bound variable at the moment it
is bound, without looking at the occurrences of that variable further in
the code. Bound variables come from function arguments, let-bindings,
and pattern matching.

### Function arguments

For function arguments, we could require every argument to be annotated with
its type. However, we can mitigate the effort by introducing several mechanisms
to save a fair number of type annotation.

One important feature is the "explicitly-requested type propagation" mechanism.
Consider a list `xs` of type `int list`. In the expression
`List.map (fun x -> x+1) xs`, one should not be required to write `fun (x:int)`
because elements from an integer list must be integers. To achieve the propagation
of information, we want to attach to the prototype
`val map : ('a -> 'a) -> 'a list -> 'a list` an indication that the instantiation
of the type variable `'a` should be inferred from the second argument, and
propagated into the first argument.

Another useful mechanism, which is known in Coq as "implicit types", would
allow to associate a default type with every variable whose name match a
given pattern. For example, every variable starting with "nb" would have
type "int" (unless specified otherwise). Note that the "implicit types"
mechanism applies only over a given scope, it should not be invasive.

This mechanism could be generalized to constants. For example, in a module
that manipulates only integer values (no double, no matrices, ...), one
could specify that `0` should be interpreted as `(0:int)`, unless another
annotation is explicitly mentioned.

When writing a larger number of types explicitly, it seems desirable to
have type variables be explicitly quantified in all polymorphic functions.
The current syntax is `(type a)`. We might want to also use `(type a b c)`.

### Let bindings

For let-bindings, we generally do not need any annotation. Indeed, in
the expression `let x = t1 in t2`, it usually suffices to type-check
`t1` for deducing a fully-resolved type for `x`.

A typical counterexample is `let r = ref []`. In this case, the type
of `r` is not fully resolved. This is a problem, for example in
`List.map (fun x -> x + 1) !r` the type of `x` would not be deducible
from the type of `!r`. In such case, it appears hard to escape an
annotation, such as: `let r = ref ([] : int list)` or
`let r : int list ref = ref []`. Both are a bit heavy, but perhaps
a lighter syntax could be devised, e.g. `let r = ref (@nil int)`
in Coq style, or `let r = ref List<int>.nil` in C++ style.

### Pattern matching

The type of variables bound by a pattern are always deducible from
the type of the value being matched. For example, if the type of `v`
is known, then in `match v with (Some x, None)::rest -> ...` the
type of `x` can be deduced.

Note that OCaml allows to infer the type of the argument from the
patterns. For example: `let f x = match x with None -> 0 | Some x -> x`
would have type `int option -> int`. But this kind of backward
propagation of information can lead to confusing error messages.



## The type inference algorithm

### First approximation

At a high-level, the type of a subexpression should always be deducible
only from that subexpression and from the typing context, which binds
each variable to its type.

As a first approximation, the typing algorithm works as follows.

- To type-check a function definition, add the arguments (whose types are
  known) to the typing context, then type-check the body.
- To type-check a let-binding, type-check the body, check that it yields
  a fully-resolved type, then add the variable to the typing context and
  type-check the continuation.
- To type-check a pattern matching, type-check the argument, check that it
  yields a fully-resolved type, then type-check each of the branches
  independently, then try to unify the type of each of the branches.
- To type-check a term coming with an explicit type annotation, type-check
  the term, then check that the result type matches the annotation.
- To type-check a record field access, e.g. `r.x`, we first resolve the
  type of `r`, check that it yields a fully-resolved type, then consider
  the field `x` associated with that record type.
- To type-check an application, first type-check the function and the
  arguments independently.
  - If the function is overloaded, e.g. `map`, then the result is a
    special token `overloaded<"map">`. We then resolve the function
    by looking among the instances associated with the symbol `map`,
    in such a way as to unify with the arguments. We require exactly
    one instance to match.
  - When the function type is known or has been resolved, we unify
    the type of each of the arguments expected by the function with
    the types of the arguments that are provided.
  - If the user has registered for that function a special type propagation
    rule (recall the example of List.map from earlier on), then we exploit
    the rule by checking the "source" arguments first, then adding a type
    annotation on the "target" arguments of the propagation.

Most of these ideas (except for the propagation rules) are detailed in the paper:
http://www.chargueraud.org/research/2015/ocaml_errors/ocaml_errors.pdf


### Overlapping instances

In a simple version, we disallow overlapping instances.
In a more refined version, we could allow overlapping instances in the cases
where the choice of the instance does not matter to the final result
(typically, different choices would lead to semantically equivalent terms.)

One question is whether overlapping can be detected at the moment of
introducing instances, or can only be detected when searching for a specific
instance. It seems straightforward to check for overlapping eagerly,
so let's try to implement this solution.


### Need for combining downward and upward propagation of information

Now, the naive algorithm described above fails to cover the common pattern
of overloaded constants. For example, in `let x : int list = [0]`, the type
of `0`  must be resolved from the expected type for the expression. As a more
interesting example, consider the expression `0 + (0 + x)`, where `0`
and `+` could denote either integer values or double values; the `0` and
the `+` must be resolved by exploiting the type of `x`. Achieving this
requires a two-pass algorithm. Let's begin with an example.

Assume a context where `x` has type `int`.
To typecheck `0 + (0 + x)`, we first type-check the arguments of the outer sum.
- To typecheck the term `0`, we return the type `?`, meaning "unable to resolve".
- To typecheck the term `0 + x`, we first type-check the arguments of that inner sum.
   - To typecheck the term `0`, we return here again the type `?`.
   - To typecheck the term `x`, we return the type `int`.
   - We then look for an instance of `+` with arguments `?` and `int` for the inner `+`.
     The only solution is to resolve to integer addition.
   - By unification, we deduce that the second `0` is of type `int`, and that
     `0 + x` has type `int`.
   - Having resolved the second `0` to have type `int`, we can restart the
     type-checking of this term, but this time with an expected return type.
   - We have to resolve `0` with expected type `int`; for this we find a
     unique solution, the zero value from integers.
- Likewise, we then look for an instance of `+` with arguments `?` and `int`
  for the outer `+`. The only solution is, here again, integer addition.
- By unification, we deduce that the first `0` is an int, and therefore must
  be the zero value from integers.

There are two key ingredients at play here. The first one is that we allow
the type-checking of a subexpression to return `?`, meaning "unable to resolve".
The second one is that we allow type-checking a "unable to resolve" subexpression
with an expected return type, so that the resolution of instances can be guided
by that expected return type. For example, an instance of `+` at type
`int -> int -> int` is selected when either (a) one of the two arguments has
type `int`, or (b) the two arguments have type `?` but the context expects
a result of type `int`. Outside of these two cases, the algorithm would simply
raise an error, refusing to make arbitrary guesses. For example, `let x = 0 + 0`
would fail to type-check without an annotation.


### Avoiding the exponential or quadratic blowup

The risk with a two-pass algorithm is the complexity blowup. Things could go
bad if we could, at each node, trigger a new downward pass all the way to the
leaves. To avoid this caveat, we impose a simple strategy: we perform
a first bottom-up pass (i.e., from leaves up to the root), then a
top-down pass (i.e., from the root down to the leaves). This strategy
may fail to typecheck certain terms that a standard unification algorithm
(Hindley-Milner's style) would have succceeded to typecheck. However, we
argue that it is fine to reject such terms: they would be fairly hard to
typechecking in one's mind, and they are fairly rare in practice.

During the bottom-up pass, instances are resolved on every application node
based on the type information available from the function and its arguments
(that is, independently of the context). The resolution may be partial,
for instance in `nil ++ nil`, the operator resolves to the polymorphic
list concatenation operation, of type `list ?a -> list ?a -> list ?a`,
in which the type `?a` is not instantiated. Nevertheless, even though
the typed is not fully resolved, the overloaded symbol `++` is resolved
from a computational perspective.

During the top-down pass, for applications whose instance had not been
resolved in the bottom-up pass, and for polymorphic constants, a resolution
is performed, exploiting the additional type information brought by the
context. For example, in `(0 : float)`, the cast operation is a context for
the zero polymorphic constants, bringing in the information that it is a
zero from float.


### Uninstantiated type variables

Consider: `length nil`. Typing should fail because `nil` has type `list ?a`
and `length` resolves to `list ?a -> int` but fails to instantiate `?a`,
and this information cannot come from the context since `a` does not appear
in the return type. One question is whether the error be reported on the
application `length nil` or on the `nil`. It seems better to locate the
errror on the first `nil`, from where `?a` originates.

Consider `nil ++ (1::nil)`. The typing of the `++` operator looks for an
instance of `list ?a -> list int -> ?r`, and finds the instance
`list int -> list int -> list int`. It then propagates downward the
information to `list int`.

Consider: `nil ++ nil`. The typing of the `++` operator looks for an
instance of `list ?a -> list ?b -> ?r` and finds the instance
`list ?a -> list ?a -> list ?a`. This does not fully resolve the type,
however the context might provide the missing information. So, the type
`list ?a` is returned for the expression. The backward pass will then
possibly fill the type `?a`. If it does not, e.g. in `length (nil ++ nil)`
then the error will ultimately propagage down to the first `nil`.
If it does, .e.g in `1::(nil ++ nil)`, then `nil ++ nil` is typechecked
against `list int`, which leads to `nil` being checked against `list int`.


### Polymorphism in instances

Different instances of a same symbol may involve various degrees of
polymorphism.

```
map : 'a. 'a list -> ('a -> 'a) -> 'a list
map : 'a 'b. ('a,'b) map -> ('a -> 'b -> 'b) -> ('a,'b) map
```

Note: in the context of OCaml, it probably makes sense to not overload
`=` and `==` (as well as `<>` and `!=`) because they are already
functions that operate on arbitrary types, and thus introducing another
instance would almost certainly introduce overlapping instances.


### Overloaded operators

`a + b` is a notation for `__operator_plus a b`. Example instances:
```
__operator_plus : int -> int -> int
__operator_plus : float -> float -> float
```
It is technically possible, though not necessarily encouraged, to define
instances of `+` for strings and lists.

`t[i]` is a notation for  `__operator_sqbracket t i`. Example instances:
```
__operator_sqbracket : string -> int -> char
__operator_sqbracket : forall 'a. 'a array -> int -> 'a
```

`t[i] <- v` is a notation for `__operator_sqbracket_assign t i v`.


`t.f` could be a reserved notation, used for module resolution (`M.t`)
and for function calls, with `t.f` being equivalent to `__f_get t` (or `f ~obj:t`).

`t.f <- v` is a notation for `__f_set t v` (or `__f_assign ~obj:t v`).

For example, the `vect` record below with 2 mutable fields would generate 4 instances:
```
type vect { mutable x : int; mutable y : int }
__x_get : vect -> int
__x_set : vect -> int -> unit
__y_get : vect -> int
__y_set : vect -> int -> unit
```


# Existing approaches to static resolution of overloading

### Parametric typeclasses  (or whatever it's called)

It is tempting to say that any type that supports a binary comparison
operator `<=` also defines implicitly a `min` operation defined as
`fun x y -> if x <= y then x else y`. This can be written as a derived
instance. In Coq's syntax:

```
Class Le (A : Type) :=
   { le : A -> A -> bool }.
Definition min `{Le A} (n m:A) : A :=
  if n <= m then n else m.
```

Experience shows that this approach tends to encourage the development
of a theory for a min operation associated with the comparison operator
`le` canonically associated with a type `A`, and prevents the sharing
of the theory of the min operation that could be applied for another
order relation on the same type `A`.

Moreover, derived instances scale up poorly, as any occurence of `min`
triggers a resolution that in turns induce a resolution of `le`.
This scheme can cascade in depth and in breadth, when piling up
typeclasses definitions with several arguments. Performances then suffer.

We find it preferable to follow a style where instances are introduced
in a forward manner, explicitly by the user, based on one's actual needs.
This approach still allows factorizing definition, via use of higher-order
results.
```
Definition min_wrt (le:A->A->bool) (n m:A) : A :=
  if n <= m then n else m.

Instance le : Le int := ..
Instance min : Min int := min_wrt le.
```
The additional cost is to declare explicitly the instances that will be used.
The gain is to avoid recursive proof search, and to factorize the theory of
min over all possible order relations, and not just the canonical order
relation for the type A.


## Typeclasses in Haskell

..

## Typeclasses in Coq

Coq features a typeclasses mechanism inspired by that of Haskell. The TLC library
(https://github.com/charguer/tlc) makes use of typeclasses for overloading symbols
(see, e.g., `LibContainerDemos.v`, `LibMap.v`, `LibSet.v`, `LibListZ.v`).
The approach succeeds in allowing to reuse the same notations on different types.
One downside, however, is that the approach introduces indirections, for example
`t[i]` is define as `read map_read_instance t i`. The latter is convertible to
the definition `Map.read t i`, however it is not syntactically the same. The
fact that a same operation can be viewed in two different ways gets in the way
of tactics such as `rewrite`. We would very much prefer an approach where
notation overloading is only a syntactic facility, and does not introduce an
indirection at the logical level.


## Encoding of typeclasses using OCaml modules

..

## Overloading of operators in C++

In C++, any function name can be overloaded for different function prototypes.
Moreover, C++ defines a fixed list of symbols that can be overloaded, i.e.,
symbols that may be viewed as function names (e.g., `operator+` is a function
name).

```
  + - * ⁄ % ‸ & | ~ ! = < > += -= *= ⁄= %= ‸= &= |=
  << >> <<= >>= == != <= >= && || ++ -- , ->* -> ( ) [ ]
  new delete new[] delete[]
```

For example, `t[i]` is a common notation to access the i-th item for all container
data structures, and `it++` is a standard syntax for stepping an iterator.

Overloading is resolved based on the type of the arguments. Afaik,
instance resolution is never guided by the expected return type.
In particular, there is no overloading for constants (e.g. `empty` or `0`).







