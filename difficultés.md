Quelques difficultés à avoir en tête

* surcharge des parenthèses et opérations de base.

Des expressions comme `a (b + c)` sont interprétées très différement suivant que `a` est une fonction ou un nombre.

Certaines notations (par exemple les intervalles à l’anglaise) surchargent les parenthèses : il n’est plus annodin de considérer ces dernières comme non-signifiantes.

* Conflit avec certaines constructions de Coq.

Par exemple la notation `>->` pour les injections.

* Coercions et surcharge d’opérateurs

On a une coercion nat → int, mais aussi deux opérateurs de comparaisons (disons `<` pour fixer les idées).
Dans ces cas là, si `a` et `b` sont des nat, alors `a < b` est ambigüe.

La bonne nouvelle, c’est que les deux interprétations sont équivalentes, mais est-ce que le typeur est censé prendre ça en compte ?

* Gestion « en arrière » des coercions

```coq
Parameters int real : Type.

Parameter convert : int -> real.
Coercion convert : int >-> real.

Parameter zero : int.
Parameter zero_dot_five : real.

Check if true then zero else zero_dot_five. (* Rejeté par Coq : "zero_dot_five" has type "real" while it is expected to have type "int". *)
```

Plus généralement, il n’est pas rare d’avoir une hiérarchie de coercions en maths, et on aimerait bien que `if b then a else c` ait le type `A ⨆ C`.
(À noter que ce n’est pas forcément possible d’automatiser la chose si cette hiérarchie n’est pas un treillis : on pourait avoir A → C, A → D, B → C, et B → D.)

Exemple sans coercion, avec 0 qui serait surchargé entier / réel :
```
f x := if ? then 0 else 0.5
```

* Ordres des coercions

Si on a une coercion `A >-> B`, alors `b = a` sera accepté, mais pas `a = b`.

* Doublons dans Unicode

Unicode a des symboles qui se ressemblent, comme Σ et ∑, et qui sont faciles à mélanger.
Exemple : vscode défini les raccourcis \sum pour ∑ et \S pour Σ.  Deplus vscode autocomplète : si on tape \s, il propose \sum et affiche ∑, qui va beaucoup ressembler à ∑.

* Comment gérer des types intermédiaires avec plusieurs interprétations ?

On peut avoir un 0 pour les entiers, les flotants, et les matrices, ainsi qu’une fonction print sur les entiers, les flotants, et les matrices.

Il y a donc plusieurs interprétations pour `print 0`… mais on aimerait ne pas avoir à dire laquelle !

* Ambiguité des mathématiques dans les chaines d’opérations

Étant donné une relation transitive, on aimerait pouvoir la chaîner :
	1 < 2 < x < y < z
	1 = x = y

Étant donné une opération associative, on aimerait pouvoir la chaîner :
	1 + 2 + x + y + z
	A ∪ B ∪ C
	f o g o h

La relation d’équivalence <=> est à la fois transitive et associative… et les deux manières de chaîner donnent des résultats bien différents :
	Bien que A <=> (B <=> C) est équivalent à (A <=> B) <=> C… quand on écrit A <=> B <=> C, on veut vraiment dire que A <=> B /\ B <=> C, ce qui est bien différent.

* Définition vs équations

En OCaml, ceci ne définit pas une fonction totale :
```ocaml
let rec f x = 1 - f x
```
Et pourtant la seule fonction respectant l’équation `f x = 1 - f x` est bien définie : c’est la fonction constante à 1/2.

* Structure sous-entendue

Il arrive que l’on sous-entend des propriétés d’un objet mathématique.
Par exemple, si on écrit P \in D, avec P un point, il est possible que D représente un ensemble quelconque de points, mais aussi que D représente une droite.

* Interactions avec les scopes

Il y a moyen d’avoir des choses assez contre-intuitives avec la sémantique des `Scope` en Coq :
```coq
From Coq Require Import Arith_base ZArith.
Local Open Scope Z_scope.

Hypothesis power : Z * N -> Z.
Notation "a ^ b" := (power (a, b%N)).

Check forall (a : Z) (b : N), a ^ b = 0. (* Accepté *)
Check forall (a : Z) (b : N), a * (a ^ b) = 0. (* Refusé : The term "b" has type "N" while it is expected to have type "Z". *)
```

Explication : ce n’est pas la même interprétation de ^, l’un est la notation juste au dessus, l’autre est Z.pow.
Comme * est dans Z_scope, il faut déclarer ^ dans Z_scope pour que cela fonctionne.
C’est très contre-intuitif, et très difficile à déboguer avec ce message d’erreur.

