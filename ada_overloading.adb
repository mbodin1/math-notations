with Text_IO; use Text_IO;


procedure ada_overloading is

	function Zero return Integer
	is 
	begin
		return 0;
	end Zero;

	function Zero return Float
	is 
	begin
		return 0.0;
	end Zero;

	function Compare (A : Integer; B : Integer) return Boolean
	is 
	begin
		return A = B;
	end Compare;

	function Compare (A : Float; B : Float) return Boolean
	is 
	begin
		return A = B;
	end Compare;


	B1 : Boolean := Compare( Zero, Integer(1) );
	B2 : Boolean := Compare( Zero, Float(1.1) );
	B3 : Boolean := Compare( Integer(1), Zero );
	B4 : Boolean := Compare( Float(1.1), Zero );

begin
   Put_Line("ok");
end ada_overloading;
