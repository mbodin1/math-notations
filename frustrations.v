
(* Ce fichier contient certaines frustrations avec le système actuel de Coq. *)

Section Singleton.

Notation "{ A }" := (A : nat).
Lemma union_singleton : forall A, {A} = {A}.
(* Error: Unknown interpretation for notation "{ _ } = { _ }". *)

End Singleton.

